/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cmw.jscompile;

import java.util.*;
import java.io.*;
/**
 *
 * @author Alexander Korzhikov <Alexander.Korzhikov@comindware.com>
 */
public class CSSFile {
    public String name;
    public String path;
    
    CSSFile( File file ) {
        this.name = file.getName();
        this.path = file.getPath();                
    }        
    
    public String toString(){
        return "<css>"+
            "\n\t<name>" + name + "</name>\n" + 
            "\n\t<name>" + path + "</path>\n" +             
        "</css>\n\n";
    }
}
