/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cmw.jscompile;

import java.util.*;
import java.io.*;
import com.google.javascript.jscomp.*;

/**
 *
 * @author Alexander Korzhikov <Alexander.Korzhikov@comindware.com>
 */
public class CompilerTask {
    public CompilerTask( Module module, String path ){
        // These are external JavaScript files you reference but don't want changed
        String externalJavascriptResources[] = {
//            "jquery.js",
//            "jqueryui.js"
        };
        // These are the files you want optimized
        Object ia[] = module.contains.toArray();   //get array
        String primaryJavascriptToCompile[] = new String[ia.length];

        if( ia.length == 0 )
            return;
        
        for( int i = 0; i < ia.length; i++ )
          primaryJavascriptToCompile[i] = (String)ia[i];
//
//        // This is where the optimized code will end up
        String outputFilename = path + "\\" + module.name + ".js";

        com.google.javascript.jscomp.Compiler compiler = new com.google.javascript.jscomp.Compiler();
        CompilerOptions options = new CompilerOptions();
    // Advanced mode is used here, but additional options could be set, too.
        CompilationLevel.ADVANCED_OPTIMIZATIONS.setOptionsForCompilationLevel( options );
        
//        options.prettyPrint = true;
//        options.ideMode = true;
//        options.setShadowVariables( true );
//        CompilationLevel.ADVANCED_OPTIMIZATIONS.setOptionsForCompilationLevel( options );
//        CompilationLevel.SIMPLE_OPTIMIZATIONS.setOptionsForCompilationLevel(options);
        CompilationLevel.WHITESPACE_ONLY.setOptionsForCompilationLevel(options);
        WarningLevel.QUIET.setOptionsForWarningLevel( options );
        
        List<JSSourceFile> externalJavascriptFiles = new ArrayList<JSSourceFile>();
        for (String filename : externalJavascriptResources)
        {
          externalJavascriptFiles.add(JSSourceFile.fromFile(filename));
        }

        List<JSSourceFile> primaryJavascriptFiles = new ArrayList<JSSourceFile>();
        for (String filename : primaryJavascriptToCompile)
        {
          primaryJavascriptFiles.add(JSSourceFile.fromFile(filename));
        }

        compiler.compile(externalJavascriptFiles, primaryJavascriptFiles, options);

        for (JSError message : compiler.getWarnings())
        {
          System.err.println("Warning message: " + message.toString());
        }

        for (JSError message : compiler.getErrors())
        {
          System.err.println("Error message: " + message.toString());
        }

        try {
            FileWriter outputFile = new FileWriter(outputFilename);
            outputFile.write( compiler.toSource() );
            outputFile.write( "Ext.require( \"Ext.app.ApplicationManager\" );" );
            outputFile.close();
        } 
        catch( IOException e ) {
        }
        
    }
}
