package cmw.jscompile;

import java.io.*;
import java.util.regex.*;
import java.util.Scanner;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Alexander Korzhikov <Alexander.Korzhikov@comindware.com>
 * @date Dec 18, 2011
 */
public class ExtJSClassParser extends Parser {
    private String currentContext = null;    
    private ArrayList methods = new ArrayList();
    private ArrayList attributes = new ArrayList();
        
    static String[] contexts = { "onLaunch", "constructor" };
    static String[] methodsTests = { 
        "^\\s*(\\w+)\\s*:\\s*function\\s*\\(.*\\)\\s*\\{\\s*$" 
    };
    static String[] attributesTests = { 
        "^\\s*(?:this|self|_self)\\.(\\w+)\\s*=.*$",
        "^\\s*(\\w+)\\s*:\\s*(?!function)(\\w+).*$"
    };
    static {
        Arrays.sort( contexts );        
    }
    
    
    ExtJSClassParser( File file ) {
        super( file );        
        result.add( methods );
        result.add( attributes );
    }
    
    public void parseFile(){
        try {
            while ( scanner.hasNextLine() ){
                parseLine( scanner.nextLine() );
            }
        }
        finally {
            scanner.close();
        }
    }
    
    public void parseLine( String line ){
        for( int i = 0; i < methodsTests.length; i++) {
            testLine( line, Pattern.compile( methodsTests[i] ), "method" );
        }
        for( int i = 0; i < attributesTests.length; i++) {
            testLine( line, Pattern.compile( attributesTests[i] ), "attribute" );
        }
    }
    
    private void testLine( String line, Pattern reg, String type ) {
        Matcher matcher = reg.matcher( line );
        if( matcher.matches() ) {
            String matchesName = matcher.group(1);
            if( type.equals( "method" ) ) {
                currentContext = matchesName;
                methods.add( matchesName );
            }
            else if( type.equals( "attribute" ) && currentContext != null && Arrays.binarySearch( contexts, currentContext ) >= 0 ){
                attributes.add( matchesName );
            }                
        }
    }
}
