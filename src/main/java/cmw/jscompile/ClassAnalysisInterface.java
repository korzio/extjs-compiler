package cmw.jscompile;
import java.util.ArrayList;

/**
 * @author Alexander Korzhikov <Alexander.Korzhikov@comindware.com>
 */
public interface ClassAnalysisInterface {
    ArrayList methods = new ArrayList<String>();
    ArrayList attributes = new ArrayList<String>();
    ArrayList relations = new ArrayList<Relation>();
    
    
    void addMethods( ArrayList containedNames );
    void addAttributes( ArrayList containedNames );
//    void addAttribute();
//    void addRelation();
}
