package cmw.jscompile;

import java.util.*;
import java.io.*;

/**
 *
 * @author Alexander Korzhikov <Alexander.Korzhikov@comindware.com>
 * @date Dec 18, 2011
 */
public class ExtJSClassAnalysis extends JSScript implements ClassAnalysisInterface {        
    ArrayList methods = new ArrayList<String>(); 
    ArrayList attributes = new ArrayList<String>(); 
    
    ExtJSClassAnalysis( File file ) {
        super( file );
        
        parser = new ExtJSClassParser( file );
        parser.parseFile();
        ArrayList result = parser.getResult();
        if( result.size() != 0 ) {
            if( result.get( 0 ) != null ) 
                addMethods( (ArrayList)result.get( 0 ) );            
            if( result.get( 1 ) != null ) 
                addAttributes( (ArrayList)result.get( 1 ) );
        }
        
    }
    
    ExtJSClassAnalysis( String filePath, String fileName ) {
        super( filePath, fileName );
    }
    
    public String toString(){
        return "\t<script>"+
            "\n\t\t<name>" + name + "</name>\n" + 
            printList( path, "path", true, "\t" ) +
            printList( methods, "method", true, "\t" ) +
            printList( attributes, "attribute", true, "\t" ) +
        "\t</script>";
    }
    
    public void addMethods( ArrayList containedNames ){
        addToContainer( methods, containedNames );
    }
        
    public void addAttributes( ArrayList containedNames ){
        addToContainer( attributes, containedNames );
    }
}
