package cmw.jscompile;

import java.io.*;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;

/**
 * @author Alexander Korzhikov <Alexander.Korzhikov@comindware.com>
 * @date Dec 18, 2011
 * simple implementation of simple parserInterface, is inherited by concrete parsers
 */
public abstract class Parser implements ParserInterface {
    protected File file;
    protected Scanner scanner;
    protected ArrayList result = new ArrayList<ArrayList>();
    
    public ArrayList getResult(){
        return result;
    };
    
    Parser( File file ) {
        file = file;
        try {
            scanner = new Scanner(new FileReader(file));
        }
        catch( FileNotFoundException e ){
            Logger.getLogger(ExtJSClassParser.class.getName()).log(Level.WARNING, "Error : scanner cannot be used");
        }
    }
    
    public void parseFile(){
        try {
            while ( scanner.hasNextLine() ){
                parseLine( scanner.nextLine() );
            }
        }
        finally {
            scanner.close();
        }
    }
    
    private void parseLine( String line ){
        
    }
}
