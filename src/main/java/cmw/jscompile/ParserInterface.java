package cmw.jscompile;

/**
 *
 * @author Alexander Korzhikov <Alexander.Korzhikov@comindware.com>
 */
public interface ParserInterface {
    void parseFile();    
}
