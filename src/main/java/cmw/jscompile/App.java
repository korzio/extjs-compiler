package cmw.jscompile;

import java.io.*;

public class App 
{
    public static void main( String[] args )
    {
        String logfile = "";
        String dir = null;
        String compiledir = null;
        String mode = null;
        String name = null;
        String configFile = null;
        String compiler = null;
        String modules = null;
        String analysis = null;
        
        for( int i = 0; i < args.length; i++ ) {
            if( i < args.length - 1 && args[i+1] != null ) {
                String flag = args[i];
                String tmp = args[i+1];
                
                if( flag.equals( "--log" ) ) {
                    logfile = tmp;
                }
                else if( flag.equals( "--dir" ) ) {
                    dir = tmp;
                }
                else if( flag.equals( "--compiler" ) ) {
                    compiler = tmp;
                }
                else if( flag.equals( "--name" ) ) {
                    name = tmp;
                }
                else if( flag.equals( "--compiledir" ) ) {
                    compiledir = tmp;
                }
                else if( flag.equals( "--mode" ) ) {
                    mode = tmp;
                }
                else if( flag.equals( "--modules" ) ) {
                    modules = tmp;
                }
                else if( flag.equals( "--config" ) ) {
                    configFile = tmp;
                }
                else if( flag.equals( "--analysis" ) ) {
                    analysis = tmp;
                }
                
                i++;
            }
        }
                
        FilesParser modulesParser = new FilesParser( logfile );
        if( analysis != null )
            modulesParser.Options.type = analysis;
        
        if( compiledir != null ) {
            File filedir = new File( dir );
            if( configFile != null ) {
                modulesParser.traverseFromFile( new File( configFile ), filedir );
            }
            else {                
                if( filedir != null )
                    modulesParser.traverse( filedir ); // real one
            }

            if( modulesParser.modules.isEmpty() ) {
                System.out.println( "Cannot create modules container" );
                return;
            }

            if( compiler != null )
                modulesParser.compiler = compiler;

            if( mode == null || mode.equals( "1" ) )
                modulesParser.compileToOne( compiledir, name == null ? "compiled" : name, modules );
            else if( mode.equals( "0" ) ) 
                modulesParser.compile( compiledir, modules );
        }
        modulesParser.close();      
    }
}