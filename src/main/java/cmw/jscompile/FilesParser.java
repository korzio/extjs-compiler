/**
 *
 * @author Alexander Korzhikov <Alexander.Korzhikov@comindware.com>
 */
package cmw.jscompile;

import java.util.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import java.util.ListIterator;


public class FilesParser {
    public static class Options {
		public String type = "default";
	}
    public static Options Options = new Options();
    
    private String indent = "";
    private String[] noModuleFolders = {
        "model", 
        "controller", 
        "widget",
        "configuration",
        "models",
        "module",
        "store",
        "view",
        "util",
        "datagetter",
        "datasetter",
        "editor",
        "app"
    };
    private BufferedWriter output;
    private File fileLog;
    private File dir;
    private Boolean flagModule = false;
//    Map<String,Module> mp;
    public String compiler = "closure";
    public String type = "js";
    ArrayList<Module> modules = new ArrayList<Module>();

    public FilesParser(String fileName)
    {
        if( !fileName.equals( "" ) ) {
            fileLog = new File( fileName );
            try {
                output = new BufferedWriter( new FileWriter( fileName ) );
            } catch (IOException e) {
                Logger.getLogger(FilesParser.class.getName()).log(Level.INFO, "cannot open file");
            }
        }
    }
    
    public FilesParser()
    {
    }
      
    public void traverse( File file )
    {
        recursiveTraversal( file, new Module( file, "" ), "" );
        dir = file;
        addRelations();
        print();
    }
    
    public void traverseFromFile( File file, File filedir ) {
        NodeList nodeLst = XMLReader.getModules( file );
        
        for (int s = 0; s < nodeLst.getLength(); s++) {
            Node fstNode = nodeLst.item(s);
            if( fstNode.getNodeType() == Node.ELEMENT_NODE ) {
                Element moduleNode = (Element) fstNode;
                
                Module module = new Module( moduleNode );
                module.addScriptsPath( filedir );
                modules.add( module );
            }
        }
        
        print();
    }
    
    public ArrayList<Module> getMap(){
        return modules;
    }
    
    private void addRelations() {
        ListIterator<Module> itr = modules.listIterator();
        while( itr.hasNext() ) {
            Module module = itr.next();
            module.normalize();            
        }
        
        ArrayList<Module> modulesNormalized = new ArrayList<Module>();
        ArrayList<String> containsAllModules = new ArrayList<String>();
        
        int number = -1;
        while( modulesNormalized.size() != modules.size() && modulesNormalized.size() != number ) {
            number = modulesNormalized.size();
            itr = modules.listIterator();
            while( itr.hasNext() ) {
                Module module = itr.next();
                if( modulesNormalized.contains( module ) )
                    continue;

                ArrayList<String> containsConcatModules = new ArrayList<String>();
                module.addToContainer( containsConcatModules, module.containsName );
                module.addToContainer( containsConcatModules, containsAllModules );

                if( module.containsAllOf( containsConcatModules, module.uses ) ) {
                    modulesNormalized.add( module );
                    module.addToContainer( containsAllModules, module.containsName );
                }
//                else {
//                    System.out.println( "module " + module.name );
//                }
            }
        }
        
        if( modulesNormalized.size() != modules.size() ) {
            itr = modules.listIterator();
            while( itr.hasNext() ) {
                Module module = itr.next();
                if( !modulesNormalized.contains( module ) ) {
                    System.out.println( "Normalized modules do not contains " + module.toString() );
                    modulesNormalized.add( module );
                }                
            }
        }
        
        modules = modulesNormalized;
    }
    
    public Boolean checkNewModule( File fileObject, String parentName, Module module ) {
        String name = fileObject.getName();
        for (int i = 0; i < noModuleFolders.length; i++) {
            if( name.equals( noModuleFolders[i] ) || parentName.equals( "common" ) || module.name.equals( "module_formeditor" ) )
                return false;
        }
        
        return true;
    }

    public void recursiveTraversal( File fileObject, Module module, String parentName ){		
        String fname = fileObject.getName();
        
        if( fname.equals( "Application.js" ) || ( parentName.equals( "common" ) && fname.equals( "form" ) ) || ( parentName.equals( "localization" ) && !fname.equals( "Localizer.js" ) ) )
            return;
        
        if( fileObject.isDirectory() && !fname.equals( ".svn" ) ){
            Module amodule;
            
            if( checkNewModule( fileObject, parentName, module ) ) {
                amodule = new Module( fileObject, parentName );
                modules.add( amodule );
            }
            else {
                amodule = module;
            }
            
            File allFiles[] = fileObject.listFiles();
            for( File aFile : allFiles ){
                String aname = aFile.getName();
                if( aname.equals( ".svn" ) )
                    continue;
                
                recursiveTraversal( aFile, amodule, fname );
            }
            
        }else if (fileObject.isFile()){
            String ext = fname
                            .substring(fname.lastIndexOf('.') + 1, fname.length());
            
            if( ext.equals( "js" ) ) {
                JSScript script = JSScriptFactory( fileObject );
                module.addScript( script );
            }
            else if( ext.equals( "css" ) && module.scripts.size() == 0 ) {                
                CSSFile css = new CSSFile( fileObject );
                module.addCSS( css );
            }
        }
    }
    
    private JSScript JSScriptFactory( File file ) {
        if( Options.type.equals( "ext" ) )
            return new ExtJSClassAnalysis( file );
        else
            return new JSScript( file );
    }
    
    public void print(){
        ListIterator<Module> it = modules.listIterator();
        log( "<?xml version=\"1.0\" ?>\n" );
        log( "<modules>\n" );
        while(it.hasNext())
        {
            Module module = it.next();
            if( !module.contains.isEmpty() ) {
                if( Options.type.equals( "ext" ) )
                    log( module.printJSScript( dir ) );
                else
                    log( module.toString( dir ) );
            }
        }
        log( "</modules>" );
    }
    
    private ArrayList<String> getItemsOnSplit( String str ){
        ArrayList<String> items = new ArrayList<String>();
        if( str != null ) {
            String arr[] = str.split( ";" );
            for (int i = 0; i < arr.length; i++) {
                items.add( arr[i] );
            }
        }
        return items;
    }
    
    public void compile( String path, String modulesToCompile ){
        ArrayList<String> items = getItemsOnSplit( modulesToCompile );
        
        //Get Map in Set interface to get key and value
        ListIterator<Module> it = modules.listIterator();
        while(it.hasNext())
        {
            Module module = it.next();
            log( module.name + "\n" );            
            
            if( items.isEmpty() || items.contains( module.name ) ) {
                if( compiler.equals( "closure" ) )
                    new CompilerTask( module, path );
                else
                    new YUICompilerTask( module, path );
            }
        }
    }
    
    public void compileToOne( String path, String name, String modulesToCompile ) {
        ArrayList<String> items = getItemsOnSplit( modulesToCompile );
        
        ListIterator<Module> it = modules.listIterator();
        Module allInOne = new Module( new File( path ), "all" );
        while(it.hasNext())
        {
            Module module = it.next();
            if( items.isEmpty() || items.contains( module.name ) ) {
                allInOne.addToContainer( allInOne.contains, module.contains );
            }
            if( module.type.equals( "css" ) )
                allInOne.type = "css";
        }
        
        allInOne.name = name;        
        if( compiler.equals( "closure" ) )
            new CompilerTask( allInOne, path );
        else
            new YUICompilerTask( allInOne, path );
    }
    
    public void close(){
        if( this.output == null )
            return;
        
        try{
            this.output.flush();
            this.output.close();
        }
        catch (IOException e) {
            Logger.getLogger(FilesParser.class.getName()).log(Level.INFO, "cannot close log file");
        }
    }
    
    
    private void log( String str ) {
        if( this.output == null )
            return;
            
        try{
            this.output.write( str );
        }
        catch( IOException e ) {
            System.out.println( "error logging" );
        }
    }
}