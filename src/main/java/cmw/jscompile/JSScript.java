/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cmw.jscompile;

import java.util.*;
import java.io.*;

/**
 *
 * @author Alexander Korzhikov <Alexander.Korzhikov@comindware.com>
 */
public class JSScript extends DependentElement {
    ArrayList<String> usedLocal = new ArrayList<String>();
    protected Parser parser;    
    
    JSScript( File file ) {
        this( file.getPath(), file.getName() );
        
        parser = new DependencyParser( file );
        parser.parseFile();
        ArrayList result = parser.getResult();
        if( !result.isEmpty() ) {
            if( result.get( 0 ) != null ) 
                uses( (ArrayList)result.get( 0 ) );
            if( result.get( 1 ) != null ) 
                contains( (ArrayList)result.get( 1 ) );
        }
    }
    
    JSScript( String filePath, String fileName ) {
        super( filePath, fileName );
    }
    
    public String toString(){
        return "<script>"+
            "\n\t<name>" + name + "</name>\n" + 
            printList( path, "path" ) +
            printList( contains, "contains" ) +
            printList( uses, "uses" ) +
        "</script>\n\n";
    }
    
    /**
     * 
     * @param {JSScript} script
     * @return {Boolean} true if this greater than given script, false otherwise
     * greater means that has to be after given
     */
    public Boolean less( JSScript script ) {
        if( script == this )
            return false;
        
        ArrayList<String> scriptUses = script.uses;
        ListIterator<String> litr = scriptUses.listIterator();
        while( litr.hasNext() ) {
            if( contains.contains( litr.next() ) ) 
                return true;
        }
        
        ArrayList<String> scriptContains = script.contains;
        litr = uses.listIterator();
        while( litr.hasNext() ) {
            if( scriptContains.contains( litr.next() ) ) 
                return false;
        }                
        
        int n1 = uses.size();
        int n2 = scriptUses.size();
        return n1 > n2 ? false : true;
    }
    
    private Boolean resolves( JSScript script ){
        ListIterator<String> litr = script.uses.listIterator();
        while( litr.hasNext() ) {
            if( contains.contains( litr.next() ) )
                return true;                
        }
        
        return false;
    }
    
    public Boolean resolveMaxThan( JSScript script, ArrayList<JSScript> scripts ) {
        ListIterator<JSScript> litr = scripts.listIterator();
        int numberScriptResolves = 0;
        int numberThisResolves = 0;
        
        while( litr.hasNext() ) {
            JSScript testScript = litr.next();
            
            if( script.resolves( testScript ) )
                numberScriptResolves++;
            if( resolves( testScript ) )
                numberThisResolves++;
        }
        
        return numberThisResolves > numberScriptResolves ? true : false;
    }
    
    public void addLocalUsed( ArrayList<String> usedInModule ) {
        ListIterator<String> usedItr = uses.listIterator();
        while( usedItr.hasNext() ) {
            String usedName = usedItr.next();
            if( usedInModule.contains( usedName ) ) {
                usedLocal.add( usedName );
            }
        }
    }
}
