/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cmw.jscompile;

import java.io.*;
import java.util.Scanner;
import java.util.regex.MatchResult;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DependencyParser extends Parser {
    private Boolean flagConstructorFound = false;

    private static void log(Object aObject){
        System.out.println(String.valueOf(aObject));
    }

    private String quote(String aText){
        String QUOTE = "'";
        return QUOTE + aText + QUOTE;
    }
    
    DependencyParser( File file ) {
        super( file );
    }
    
    private void parseResult( String type, String name, ArrayList uses, ArrayList contains ){
        if( name != null && type != null && !type.equals( "" ) && !name.equals( "" ) && !name.matches( "^Ext.*$" ) && ( name.matches( "^[A-Za-z]+\\.([A-Za-z]+\\.?)+$" ) || name.matches( "^FC$" ) ) ){
            if( type.equals( "define" ) || type.equals( "create" ) || type.equals( "alternateClassName" ) ) {
//            if( type.equals( "define" ) || type.equals( "alternateClassName" ) ) {
                contains.add( name );
            }
            else {
                uses.add( name );
            }
        }
    }
    
    public void parseFile(){
        String []processLineResult;        
        ArrayList uses = new ArrayList();
        ArrayList contains = new ArrayList();
        result.add( uses );
        result.add( contains );
        
        try {
            //first use a Scanner to get each line
            while ( scanner.hasNextLine() ){
                processLineResult = processLine( scanner.nextLine() );
                if( flagConstructorFound )
                    return;
                    
                String type = processLineResult[0];
                String name = processLineResult[1];
                String first = processLineResult[2];
                String last = processLineResult[3];
                
                parseResult( type, name, uses, contains );
                if( type != null && first != null && ( first.equals( "[" ) || first.equals( "{" ) ) && ( last == null || ( !last.equals( "]" ) || !last.equals( "}" ) ) ) && scanner.hasNextLine() ) {
                    String str = scanner.nextLine();
                    processLineResult = processNextLine( str );
                    if( flagConstructorFound )
                        return;
                    
                    while( !str.matches( ".*[\\]\\}].*" ) && scanner.hasNextLine() ) {
                        parseResult( type, processLineResult[1], uses, contains );
                        
                        str = scanner.nextLine();
                        processLineResult = processNextLine( str );
                        if( flagConstructorFound )
                            return;
                    }
                    
                    parseResult( type, processLineResult[1], uses, contains );
                }
            }
        }
        finally {
            scanner.close();
        }
    }
    
    private void parseLine( String line ){
        
    }
  
    /** Template method that calls {@link #processLine(String)}.  
     * @throws FileNotFoundException 
     */
    public final ArrayList[] processLineByLine() throws FileNotFoundException {
        //Note that FileReader is used, not File, since File is not Closeable
        Scanner scanner = new Scanner(new FileReader(file));
        String []processLineResult;
        
        
        ArrayList uses = new ArrayList();
        ArrayList contains = new ArrayList();
        
        ArrayList []result = { uses, contains };
        
            try {
            //first use a Scanner to get each line
            while ( scanner.hasNextLine() ){
                processLineResult = processLine( scanner.nextLine() );
                if( flagConstructorFound )
                    return result;
                    
                String type = processLineResult[0];
                String name = processLineResult[1];
                String first = processLineResult[2];
                String last = processLineResult[3];
                
                parseResult( type, name, uses, contains );
                if( type != null && first != null && ( first.equals( "[" ) || first.equals( "{" ) ) && ( last == null || ( !last.equals( "]" ) || !last.equals( "}" ) ) ) && scanner.hasNextLine() ) {
                    String str = scanner.nextLine();
                    processLineResult = processNextLine( str );
                    if( flagConstructorFound )
                        return result;
                    
                    while( !str.matches( ".*[\\]\\}].*" ) && scanner.hasNextLine() ) {
                        parseResult( type, processLineResult[1], uses, contains );
                        
                        str = scanner.nextLine();
                        processLineResult = processNextLine( str );
                        if( flagConstructorFound )
                            return result;
                    }
                    
                    parseResult( type, processLineResult[1], uses, contains );
                }
            }
        }
        finally {
            scanner.close();
            return result;
        }
    }
    
    private Boolean assertLine( Scanner scanner ) {
        if( scanner.findInLine( ".*constructor.*" ) != null ) {
            flagConstructorFound = true;
            return true;
        }
        return false;
    }
  
    protected String[] processNextLine( String aLine ){
        MatchResult mr;
        String []result = {"", "", "", ""};
        
        Scanner scanner = new Scanner( aLine );
        if( assertLine( scanner ) ) {
            return result;
        }
        if( scanner.findInLine( "[\"\\'](.*)[\"\\'][\\,]?([\\]\\}])?" ) != null ) {
            mr = scanner.match();
            if( mr.group( 1 ).matches( "^([A-Za-z]+\\.?)+$" ) ) {
                result[0] = "";
                result[1] = mr.group( 1 );
                result[2] = "";
                result[3] = mr.group( 2 );
            }
        }
        
        scanner.close(); 
        return result;
    }
    
    protected String[] processLine(String aLine){
        MatchResult mr;
        String []result = {"", "", "", ""};
        
        Scanner scanner = new Scanner( aLine );
        if( assertLine( scanner ) ) {
            return result;
        }
        scanner.reset();
        
        if( scanner.findInLine( "(define|extend|toolbar|alternateClassName|models|views|requires|controllers)\\s*[\\:\\.\\(]+\\s*([\\[\\{])?[\"\\']?([^\\']+)?[\"\\']?\\s*([\\]\\}])?" ) != null ) {
            mr = scanner.match();
            if( mr.group( 3 ) == null || mr.group( 3 ).matches( "\\s*" ) || ( !mr.group( 3 ).matches( "Ext\\..*" ) && mr.group( 3 ).matches( "^([A-Za-z]+\\.?)+$" ) ) ) {
                result[0] = mr.group( 1 );
                result[1] = mr.group( 3 );
                result[2] = mr.group( 2 );
                result[3] = mr.group( 4 );
            }                  
        }

        scanner.close(); 
        return result;
    }
}