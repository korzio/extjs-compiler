/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cmw.jscompile;

import java.util.*;
import java.io.*;
import com.yahoo.platform.yui.compressor.JavaScriptCompressor;
import com.yahoo.platform.yui.compressor.CssCompressor;
import org.mozilla.javascript.ErrorReporter;
import org.mozilla.javascript.EvaluatorException;

/**
 *
 * @author Alexander Korzhikov <Alexander.Korzhikov@comindware.com>
 */
public class YUICompilerTask {        
    
    public YUICompilerTask( Module module, String path ) {
    	try {
			Options o = new Options(); // use defaults
            Object ia[] = module.contains.toArray();   //get array
            String primaryFileNamesToCompile[] = new String[ia.length];

            if( ia.length == 0 )
                return;
            for( int i = 0; i < ia.length; i++ )
              primaryFileNamesToCompile[i] = (String)ia[i];
            
            if( module.type.equals( "css" ) )
                o.type = "css";
            
            String outputFilename = path + "/" + module.name + ( module.type.equals( "css" ) ? ".css" : ".js" );
            
            YUICompilerTask.compress( primaryFileNamesToCompile, outputFilename, o );
		} catch (Exception e) {
			logger( e.getMessage() );
		}
    }

	public static void compress(String []inputFilenames, String outputFilename, Options o) throws IOException {
		Reader in = null;
		Writer out = null;
        
        try {
			out = new OutputStreamWriter(new FileOutputStream(outputFilename), o.charset);
            
            for (int i = 0; i < inputFilenames.length; i++) {
                in = new InputStreamReader(new FileInputStream(inputFilenames[i]), o.charset);
                if( o.type.equals( "css" ) ) {
                    CssCompressor compressor = new CssCompressor( in );
                    compressor.compress( out, o.lineBreakPos );
                }
                else {
                    JavaScriptCompressor compressor = new JavaScriptCompressor( in, new YuiCompressorErrorReporter() );
                    compressor.compress(out, o.lineBreakPos, o.munge, o.verbose, o.preserveAllSemiColons, o.disableOptimizations);
                }
                
                in.close(); 
                in = null;
            }						
		} 
        finally {
            out.flush(  );
            if( o.type.equals( "js" ) )
                out.write( "Ext.require( \"ApplicationManager\" );" );
            
			out.close();
		}
	}

	public static class Options {
		public String charset = "UTF-8";
		public String type = "js";
		public int lineBreakPos = -1;
		public boolean munge = true;
		public boolean verbose = false;
		public boolean preserveAllSemiColons = false;
		public boolean disableOptimizations = false;
	}
	
	private static class YuiCompressorErrorReporter implements ErrorReporter {
		public void warning(String message, String sourceName, int line, String lineSource, int lineOffset) {
			if (line < 0) {
				logger( message);
			} else {
				logger( line + ':' + lineOffset + ':' + message);
			}
		}

		public void error(String message, String sourceName, int line, String lineSource, int lineOffset) {
			if (line < 0) {
				logger( message);
			} else {
				logger( line + ':' + lineOffset + ':' + message );
			}
		}

		public EvaluatorException runtimeError(String message, String sourceName, int line, String lineSource, int lineOffset) {
			error(message, sourceName, line, lineSource, lineOffset);
			return new EvaluatorException(message);
		}
	}
    
    private static void logger( String string ) {
        System.out.println( string );
    }
}
