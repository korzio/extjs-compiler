/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cmw.jscompile;

import java.util.ArrayList;
import java.util.ListIterator;
import org.w3c.dom.Element;

/**
 * @author Alexander Korzhikov <Alexander.Korzhikov@comindware.com>
 */
public class DependentElement implements DependentElementInterface {    
    ArrayList uses = new ArrayList<String>(); // used names
    ArrayList contains = new ArrayList<String>(); // defined names
    ArrayList path = new ArrayList<String>(); // existing path
    String name = new String();
    
    DependentElement( String path, String name ){
        addPath( path );
        this.name = name;
    }
    
    DependentElement( Element node ){}
    
    protected Boolean containsAllOf( ArrayList listwho, ArrayList listwhat ) {
        ListIterator<String> litr = listwhat.listIterator();
        while( litr.hasNext() ) {
            String test = litr.next();
            if( !listwho.contains( test ) ) {                
//                System.out.println( "modules do not contain " + test );
                return false;
            }
        }
        
        return true;
    }
        
    protected void addToContainer( ArrayList container, String element ) {
        if( !container.contains( element ) )
            container.add( element );
    }
    protected void addToContainer( ArrayList listto, ArrayList listfrom ) {
        ListIterator<String> litr = listfrom.listIterator();
        while( litr.hasNext() ) {
            String name = litr.next();
            if( !listto.contains( name ) )
                listto.add( name );
        }
    }
    
    
    protected String printList( ArrayList list, String prefix, Boolean flag, String pre ) {
        String result = "";
        ListIterator<String> litr = list.listIterator();
        while( litr.hasNext() ) {
            String element = litr.next();
            result += pre + "\t<" + prefix + ">" + element + "</" + prefix + ">\n";
        }
        
        return result;
    }
    
    protected String printList( ArrayList list, String prefix ) {
        String result = "";
        ListIterator<String> litr = list.listIterator();
        while( litr.hasNext() ) {
            String element = litr.next();
            result += "\t<" + prefix + ">" + element + "</" + prefix + ">\n";
        }
        
        return result;
    }
    
    protected String printList( ArrayList list, String prefix, String exclude ) {
        String result = "";
        ListIterator<String> litr = list.listIterator();
        while( litr.hasNext() ) {
            String element = litr.next();
            result += "\t<" + prefix + ">" + element.replace( exclude, "" ) + "</" + prefix + ">\n";
        }
        
        return result;
    }
    
    public void uses( String usedName ){
        addToContainer( uses, usedName );
    }
    public void uses( ArrayList usedNames ){
        addToContainer( uses, usedNames );
    }
    
    public void contains( String containedName ){
        addToContainer( contains, containedName );
    }
    public void contains( ArrayList containedNames ){
        addToContainer( contains, containedNames );
    }        
    
    public void addPath( String path ){
        addToContainer( this.path, path );
    }
    public void addPath( ArrayList paths ){
        addToContainer( this.path, paths );
    }
    
    public String toString(){
        return "<module>"+
                    "\n\t<moduleName>" + name + "</moduleName>" + 
                    "\n" + 
                    printList( path, "path" ) +
                    printList( contains, "contains" ) +
                    printList( uses, "uses" ) +
                "</module>\n\n";
    }
    
    public void print(){
        System.out.println( toString() );
    }
}
