/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cmw.jscompile;

import java.util.ArrayList;
import java.util.ListIterator;
import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import java.util.Collections;
import java.util.Comparator;
import org.w3c.dom.NodeList;

/**
 * @author Alexander Korzhikov <Alexander.Korzhikov@comindware.com>
 */
public class Module extends DependentElement {
    ArrayList scripts = new ArrayList<JSScript>();
    ArrayList dependentModules = new ArrayList<String>();
    ArrayList containsName = new ArrayList<String>();
    public String type = "js";
    
    Module( Element node ) {
        super( node );
        
        NodeList fstNmElmntLst = node.getElementsByTagName("name");
        Element fstNmElmnt = (Element) fstNmElmntLst.item(0);
        NodeList fstNm = fstNmElmnt.getChildNodes();
        this.name = ((Node) fstNm.item(0)).getNodeValue();
        
        fstNmElmntLst = node.getElementsByTagName("path");
        fstNmElmnt = (Element) fstNmElmntLst.item(0);
        fstNm = fstNmElmnt.getChildNodes();
        String path = ((Node) fstNm.item(0)).getNodeValue();
        if( path != null )
            addPath( path );
        
        NodeList scriptLst = node.getElementsByTagName( "script" );
        if( scriptLst.getLength() != 0 )
            for (int s = 0; s < scriptLst.getLength(); s++) {
                Node scriptNode = scriptLst.item(s);
                if( scriptNode.getNodeType() == Node.ELEMENT_NODE ) {
                    Element scriptElement = (Element) scriptNode;
                    NodeList scriptPath = scriptElement.getChildNodes();
                    contains.add( ((Node) scriptPath.item(0)).getNodeValue() );
                    System.out.println( ((Node) scriptPath.item(0)).getNodeValue() );
                }
            }
        else {
            NodeList cssLst = node.getElementsByTagName( "css" );
            if( cssLst.getLength() != 0 ) {
                for (int s = 0; s < cssLst.getLength(); s++) {
                    Node cssNode = cssLst.item(s);
                    if( cssNode.getNodeType() == Node.ELEMENT_NODE ) {
                        Element cssElement = (Element) cssNode;
                        NodeList cssPath = cssElement.getChildNodes();
                        contains.add( ((Node) cssPath.item(0)).getNodeValue() );
                        System.out.println( ((Node) cssPath.item(0)).getNodeValue() );
                    }
                }
                type = "css";
            }
        }
        
        scriptLst = node.getElementsByTagName( "uses" );
        for (int s = 0; s < scriptLst.getLength(); s++) {
            Node scriptNode = scriptLst.item(s);
            if( scriptNode.getNodeType() == Node.ELEMENT_NODE ) {
                Element scriptElement = (Element) scriptNode;
                NodeList scriptPath = scriptElement.getChildNodes();
                uses.add( ((Node) scriptPath.item(0)).getNodeValue() );
//                System.out.println( ((Node) scriptPath.item(0)).getNodeValue() );
            }
        }
        scriptLst = node.getElementsByTagName( "contains" );
        for (int s = 0; s < scriptLst.getLength(); s++) {
            Node scriptNode = scriptLst.item(s);
            if( scriptNode.getNodeType() == Node.ELEMENT_NODE ) {
                Element scriptElement = (Element) scriptNode;
                NodeList scriptPath = scriptElement.getChildNodes();
                containsName.add( ((Node) scriptPath.item(0)).getNodeValue() );
//                System.out.println( ((Node) scriptPath.item(0)).getNodeValue() );
            }
        }
    }
    
    Module( File file, String parentName ) {
        this( file.getPath(), ( parentName.equals( "module" ) ? "module_" : "" ) + file.getName() );
//        print();
    }
    
    Module( String filePath, String fileName ) {
        super( filePath, fileName );
    }    
    
    private String printScriptsList() {
        String result = "";
        ListIterator<JSScript> litr = scripts.listIterator();
        while( litr.hasNext() ) {
            result += litr.next().toString();
        }
        
        return result;
    }
    
    public String toString(){
        return "<module>"+
            "\n\t<name>" + name + "</name>\n" + 
            printList( path, "path" ) +
            printList( contains, "script" ) +
            printList( containsName, "contains" ) +
            printList( uses, "uses" ) +
            printList( dependentModules, "dependentModules" ) +
        "</module>\n\n";
    }
    
    public String toString( File file ){
        if( file == null )
            return toString();
        
        return "<module>"+
            "\n\t<name>" + name + "</name>\n" + 
            printList( path, "path", file.getPath() ) +
            printList( contains, "script", file.getPath() ) +
            printList( containsName, "contains" ) +
            printList( uses, "uses" ) +
            printList( dependentModules, "dependentModules" ) +
        "</module>\n\n";
    }
    
    public String printJSScript( File file ) {
        if( file == null )
            return toString();
        
        String result = "";
        
        result = "<module>" + "\n\t<name>" + name + "</name>\n" + printList( path, "path", file.getPath() );
        ListIterator<JSScript> litr = scripts.listIterator();
        while( litr.hasNext() ) {
            JSScript element = litr.next();
            result += element.toString() + "\n";
        }
        result += "</module>\n\n";
        
        return result;
    }
    
    public void normalize(){
        ListIterator<String> cssFlagItr = contains.listIterator();
        Boolean flagCss = true;
        while( cssFlagItr.hasNext() ) {
            String contain = cssFlagItr.next();
            if( !contain.contains( ".css" ) ) {
                flagCss = false;
                break;
            }            
        }
        if( flagCss && contains.size() != 0 ) {
            type = "css";
            return;
        }
        
        containsName.clear();
        ListIterator<JSScript> scriptItr = scripts.listIterator();
        while( scriptItr.hasNext() ) {
            JSScript script = scriptItr.next();
            addToContainer( containsName, script.contains );
        }
        
        ArrayList<String> usesLocal = containsName;
        scriptItr = scripts.listIterator();
        while( scriptItr.hasNext() ) {
            JSScript script = scriptItr.next();
            script.addLocalUsed( usesLocal );
        }
        
        Collections.sort( scripts, new JSLocalUsesComparator() );
        ArrayList<JSScript> scriptsNormalized = new ArrayList<JSScript>();
        ArrayList<String> containsAlready = new ArrayList<String>();
        
        while( scriptsNormalized.size() != scripts.size() ) {
            scriptItr = scripts.listIterator();
            while( scriptItr.hasNext() ) {
                JSScript script = scriptItr.next();
                
                ArrayList<String> scriptUsesLocal = script.usedLocal;
                if( !scriptsNormalized.contains( script ) && ( scriptUsesLocal.isEmpty() || containsAllOf( containsAlready, scriptUsesLocal ) ) ) {
                    scriptsNormalized.add( script );
                    addToContainer( containsAlready, script.contains );
                }
            }
        }
        
        scripts = scriptsNormalized;
        uses.clear();
        contains.clear();
        scriptItr = scripts.listIterator();
        while( scriptItr.hasNext() ) {
            JSScript script = scriptItr.next();
            addToContainer( uses, script.uses );
            addToContainer( contains, script.path );
            addToContainer( containsName, script.contains );
        }
    }
    
    
    
    public void sortMaxResolves( ArrayList<JSScript> scripts ) {
        ArrayList<JSScript> scriptsLocal = new ArrayList<JSScript>();
        ListIterator<JSScript> litr = scripts.listIterator();
        while( litr.hasNext() ) {
            scriptsLocal.add( litr.next() );
        }
        
        for( int i = 0; i < scripts.size(); i++ ) {
            int max = i;
            JSScript script1 = scripts.listIterator( i ).next();
            JSScript scriptMaxResolves = script1;
            
            for( int j = i + 1; j < scripts.size(); j++) {                     
                JSScript script2 = (JSScript)scripts.listIterator( j ).next();
                if( script2.resolveMaxThan( scriptMaxResolves, scriptsLocal ) ) {
                    scriptMaxResolves = script2;
                    max = j;
                }
            }
            
            if( max != i ) {
                scripts.set( i, scriptMaxResolves );
                scripts.set( max, script1 );
                scriptsLocal.remove( scriptMaxResolves );
            }
        }
        
    }
    
    public void addDependenceModule( ArrayList<String> modulePath ) {
        addToContainer( dependentModules, modulePath );
    }
    
    public ArrayList getUses(){
        return uses;
    }
    
    public ArrayList getContains(){
        return containsName;
    }
    
    public void addScript( JSScript script ) {
        scripts.add( script );
        
        uses( script.uses );
        contains( script.path );
    }
    
    public void addCSS( CSSFile css ) {
        contains( css.path );
//        type = "css";
    }
    
    public void addScriptsPath( File filedir ) {
        if( filedir == null )
            return;
        
        ListIterator<String> litr = contains.listIterator();
        ArrayList<String> newcontains = new ArrayList<String>();
        String path = filedir.getPath();
        while( litr.hasNext() ) {
            String script = litr.next();
            script = path + script;
            newcontains.add( script );
        }
        
        contains = newcontains;
    }
    
}

// compares by local uses 
class JSLocalUsesComparator implements Comparator<JSScript> {
    public int compare(JSScript s1, JSScript s2) {
        ArrayList<String> usedLocal1 = s1.usedLocal;
        ArrayList<String> usedLocal2 = s2.usedLocal;

        return usedLocal1.size() > usedLocal2.size() ? 1 : -1;
    }
}