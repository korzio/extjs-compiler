/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cmw.jscompile;

import java.util.ArrayList;

/**
 *
 * @author Alexander Korzhikov <Alexander.Korzhikov@comindware.com>
 */
public interface DependentElementInterface {
    ArrayList uses = new ArrayList<String>(); // used names
    ArrayList contains = new ArrayList<String>(); // defined names
    ArrayList path = new ArrayList<String>(); // existing path
    
    void uses( String usedName );
    void contains( String containedName );
    void addPath( String path );
}
