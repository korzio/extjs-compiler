java -cp "jscompile\trunk\yuicompressor-2.4.7.jar;jscompile\trunk\js-1.6R7.jar;jscompile\trunk\jscompile-1.0.jar" cmw.jscompile.App ^
--config "C:\tmp\log.xml" ^
--modules "module_cmwobject;module_common;module_itemcontainer;module_querybuilder;module_search;module_workspace;workspace;desktop;module_analytics;module_dashboard;module_dataset;module_form" ^
--name "compiled_uYUI" ^
--compiler "YUICompressor" ^
--dir "C:\trunk\components\Platform\Server\Site\app" ^
--compiledir "C:\trunk\components\Platform\Server\Site\compiled"